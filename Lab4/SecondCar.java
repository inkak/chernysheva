package Lab4;

/**
 * Created by inkak on 4/15/2016.
 */
public class SecondCar extends GeneralCar {

    SecondCar(double speedMax, double acceleration, double maneuverability, String name){
        super(speedMax, acceleration, maneuverability, name);
    }
    @Override
    protected void turn() {
        speedCurr=speedCurr+acceleration*time;
        if(speedCurr<0.5*speedMax){
            acceleration=acceleration*2;
            System.out.println(acceleration);
        }
        speedCurr=speedCurr*maneuverability;
    }
    @Override
    protected void drive() {
        time= (int) (time+(Math.sqrt(speedCurr*speedCurr+2*acceleration*track)-speedCurr)/acceleration);
    }
}
