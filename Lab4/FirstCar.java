package Lab4;

/**
 * Created by inkak on 4/15/2016.
 */
public class FirstCar extends GeneralCar {

    FirstCar(double speedMax, double acceleration, double maneuverability, String name){
        super(speedMax, acceleration, maneuverability, name);
    }

    @Override
    protected void turn() {
        speedCurr=speedCurr+acceleration*time;
        if(speedCurr>0.5*speedMax){
            maneuverability=(maneuverability+(((speedCurr-0.5*speedMax)*0.5)/100));
        }
        speedCurr=speedCurr*maneuverability;
        maneuverability=temp1;
    }
    @Override
    protected void drive() {
        time=Math.round(time+(Math.sqrt(speedCurr*speedCurr+2*acceleration*track)-speedCurr)/acceleration);
    }

}
