package Lab4;

/**
 * Created by inkak on 4/15/2016.
 */
public class ThirdCar extends GeneralCar{

    ThirdCar(double speedMax, double acceleration, double maneuverability, String name){
        super(speedMax, acceleration, maneuverability, name);
    }
    @Override
    protected void turn() {
        speedCurr=speedCurr+acceleration*time;
        if (speedCurr==speedMax){
        speedMax=speedMax+speedMax*0.1;
        }
        speedCurr=speedCurr*maneuverability;
    }
    @Override
    protected void drive() {
        time=Math.round(time+(Math.sqrt(speedCurr*speedCurr+2*acceleration*track)-speedCurr)/acceleration);
    }
}
