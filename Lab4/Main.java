package Lab4;

import java.util.Arrays;

/**
 * Created by inkak on 4/15/2016.
 */
public class Main {
    public static void main(String[] args) {
        GeneralCar car1=new FirstCar(300, 2.3, 0.1, "#1");
        GeneralCar car2=new SecondCar(180, 2.6, 0.5, "#2");
        GeneralCar car3=new ThirdCar(250, 2.8, 0.7, "#3");
        GeneralCar[] masTemp=new GeneralCar[3];
        masTemp[0]=car1;
        masTemp[1]=car2;
        masTemp[2]=car3;
        String result;

        for(GeneralCar x:masTemp){
                for (int i = 0; i < 20; i++) {
                    if (x.speedCurr >= x.speedMax) {
                        x.speedCurr = x.speedMax;
                        x.drive();
                        x.turn();
                    } else {
                        x.drive();
                        x.turn();

                    }
                }
            }
         for (int i = 0; i < masTemp.length; i++) {
            for (int j = 0; j < masTemp.length; j++) {
                if(masTemp[i].time<masTemp[j].time){
                    GeneralCar temp1=masTemp[i];
                    masTemp[i]=masTemp[j];
                    masTemp[j]=temp1;
                }
            }
        }

        for (int t=0; t<masTemp.length; t++){
            int roundedTimeMin=(int)((masTemp[t].time/60)%60);
            int roundedTimeSec=(int)((masTemp[t].time)%60);
            result = roundedTimeMin+" минут "+ roundedTimeSec +" секунд";
            System.out.println("Автомобиль "+masTemp[t].name+ " прошел трассу за " + result);
        }
    }
}
