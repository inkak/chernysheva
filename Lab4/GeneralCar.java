package Lab4;

/**
 * Created by inkak on 4/15/2016.
 */
public abstract class GeneralCar {
    double speedMax;
    double acceleration;
    double maneuverability;
    double speedCurr;
    double time=0;
    int track = 2000;
    double temp1=maneuverability;
    String name;


    public GeneralCar(double speedMax, double acceleration, double maneuverability, String name){
        this.speedMax=speedMax;
        this.acceleration=acceleration;
        this.maneuverability=maneuverability;
        this.name=name;
    }
    protected abstract void turn();
    protected abstract void drive();

}
