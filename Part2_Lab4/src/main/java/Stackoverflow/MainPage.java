package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

/**
 * Created by Inka on 04.06.2016.
 */
public class MainPage {
    public WebDriver driver;

    public MainPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }
    @FindBy(xpath = ".//*[@id='tabs']//span")
    public WebElement featuredCount;
    public int count;

    public void checkCount(){
        String s=featuredCount.getText();
        count=Integer.parseInt(s);
        System.out.println(count);
    }

    @FindBy(xpath = "html/body//span/a[contains(text(),'sign up')]")
    public WebElement signUp;

    public SignUpPage signUpClick(){
        signUp.click();
        return new SignUpPage(driver);
    }

    @FindAll(@FindBy(how = How.XPATH, using = ".//div[contains(@class,'question-summary')]//h3/a"))
    public List<WebElement> questionClick;

    public QuestionPage randomQuestionClick(){
        int randomRec=(new Random()).ints(1, questionClick.size()).iterator().nextInt();
        int i=0;
        for (WebElement elem: questionClick) {
            if(i==randomRec) {
                elem.click();
                System.out.println(randomRec);
                break;
            }
            else
                i++;
        }
        return new QuestionPage(driver);
    }

    @FindAll(@FindBy(how = How.XPATH, using = ".//div[@id='hireme']//div[@class='title']"))
    public List<WebElement> pathsList;
    public int jobSug;
    public void salarySuggestion(){
        int [] x=new int[pathsList.size()];
        int i=0;
        for(WebElement elem: pathsList){
            String s1=elem.getText().replaceAll("[^\\$(\\d*)]", "");
            String s2=s1.trim().replaceAll("[^0-9]", "");
            if(s2.equals("")){
                x[i]=0;
                i++;
            }
            else{
                int pay=Integer.parseInt(s2);
                x[i]=pay;
                i++;
            }
        }
        jobSug=0;
        for(int j=0;j<x.length;j++){
            if(x[j]>100){
                jobSug=x[j];
            }
        }
    }

}
