import Stackoverflow.MainPage;
import Stackoverflow.QuestionPage;
import Stackoverflow.SignUpPage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Inka on 04.06.2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    protected static MainPage mainPage;
    protected static SignUpPage signUpPage;
    protected static QuestionPage questionPage;

    @BeforeClass
    public static void beforeAll() throws Exception{
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        mainPage=new MainPage(driver);
    }
    @Before
    public void before() throws Exception{
        driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void afterAll() throws Exception {
        driver.close();
    }

    @Test
    public void test1_CheckFeaturedCount() throws Exception {
        mainPage.checkCount();
        assertTrue("Element 'featured tab' wasn't found", mainPage.count > 300);
    }
    @Test
    public void test2_CheckSignUp()throws Exception {
        signUpPage=mainPage.signUpClick();
        signUpPage.buttonList();
        assertTrue("Buttons are not found", signUpPage.buttons.contains("Google") && signUpPage.buttons.contains("Facebook"));
    }
    @Test
    public void test3_CheckQuestionClick() throws Exception{
        questionPage=mainPage.randomQuestionClick();
        assertTrue("Question was asked not today", questionPage.checkDateQ.getText().equals("today"));
    }
    @Test
    public void test4_CheckSalarySuggestion() throws Exception{
        assertTrue("There is no job suggestions with such payment", mainPage.jobSug>100);
    }
}
