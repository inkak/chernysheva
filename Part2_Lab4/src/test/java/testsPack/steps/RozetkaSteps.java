package testsPack.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import testsPack.runner.Runner;

import static org.junit.Assert.assertTrue;

/**
 * Created by Inka on 04.06.2016.
 */
public class RozetkaSteps {
    @Given("^I on Rozetka main page$")
    public void iOnRozetkaMainPage() throws Throwable {
        Runner.driver.get("http://rozetka.com.ua/");
        assertTrue("We are not on main Rozetka page", Runner.driver.findElement(By.xpath(".//*[@id='head_banner_container']//div/ul/li/span[@class=\"novisited m-tabs-link active\"]")).isDisplayed());
    }

    @Then("^I see logo page title$")
    public void iSeeLogoPageTitle() throws Throwable {
        assertTrue("Element 'logo Rozetka' wasn't found", Runner.mainPage.logoRozetka.isDisplayed());
    }

    @Then("^I see menu item 'Apple'$")
    public void iSeeMenuItemApple() throws Throwable {
        assertTrue("Item of menu 'Apple' doesn't exist", Runner.mainPage.productCatalog.getText().contains("Apple"));
    }

    @Then("^I see 'MP(\\d+)' in poduct menu$")
    public void iSeeMPInPoductMenu(int arg0) throws Throwable {
        assertTrue("Can't find 'MP3' element in menu", Runner.mainPage.itemMenu.getText().contains("MP3"));
    }

    @When("^I click on link 'Выберете город'$")
    public void iClickOnLinkВыберетеГород() throws Throwable {
        Runner.citiesPopup=Runner.mainPage.navigateToPopup();
    }

    @And("^I see the pop-up menu$")
    public void iSeeThePopUpMenu() throws Throwable {
        assertTrue("We are not on main Rozetka page", Runner.driver.findElement(By.xpath(".//*[@id='city-chooser']//div[@class=\"popup-css header-city-choose-popup\"]")).isDisplayed());
        Runner.citiesPopup.citiesList();
    }

    @Then("^I see links 'Киев', 'Харьков', 'Одесса'$")
    public void iSeeLinksКиевХарьковОдесса() throws Throwable {
        assertTrue("Can't find 'Киев', 'Одесса', 'Харьков'", Runner.citiesPopup.cities.contains("Киев") && Runner.citiesPopup.cities.contains("Одесса")&& Runner.citiesPopup.cities.contains("Харьков"));
    }

    @When("^I click on cart link$")
    public void iClickOnCartLink() throws Throwable {
        Runner.cartPage=Runner.mainPage.navigateToCartPage();
    }

    @Then("^I see cart page$")
    public void iSeeCartPage() throws Throwable {
        assertTrue("Cart is not empty", Runner.driver.findElement(By.xpath(".//*[@id='cart-popup']")).isDisplayed());
    }

    @And("^I see cart is empty$")
    public void iSeeCartIsEmpty() throws Throwable {
        assertTrue("Cart is not empty", Runner.cartPage.emptyCart.getText().contains("Корзина пуста"));
    }
}
