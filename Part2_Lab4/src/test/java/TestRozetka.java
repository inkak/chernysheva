import Rozetka.CartPage;
import Rozetka.CitiesPopup;
import Rozetka.MainPage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by Inka on 04.06.2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    protected static MainPage mainPage;
    protected static CitiesPopup citiesPopup;
    protected static CartPage cartPage;

    @BeforeClass
    public static void beforeAll() throws Exception{
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        mainPage=new MainPage(driver);
    }
    @Before
    public void before() throws Exception{
        driver.get("http://rozetka.com.ua/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void after(){
        driver.close();
    }

    @Test
    public void test1_logoRozetka(){
        assertTrue("Element 'logo Rozetka' wasn't found", mainPage.logoRozetka.isDisplayed());
    }
    @Test
    public void test2_CheckProductCatalog(){
        assertTrue("Item of menu 'Apple' doesn't exist", mainPage.productCatalog.getText().contains("Apple"));
    }
    @Test
    public void test3_menuItemMP3(){
        assertTrue("Can't find 'MP3' element in menu", mainPage.itemMenu.getText().contains("MP3"));
    }
    @Test
    public void test4_CheckChangeCity(){
        citiesPopup=mainPage.navigateToPopup();
        citiesPopup.citiesList();
        assertTrue("Can't find 'Киев', 'Одесса', 'Харьков'", citiesPopup.cities.contains("Киев") && citiesPopup.cities.contains("Одесса")& citiesPopup.cities.contains("Харьков"));
    }
    @Test
    public void test5_CheckCartIsEmpty(){
        cartPage=mainPage.navigateToCartPage();
        assertTrue("Cart is not empty", cartPage.emptyCart.getText().contains("Корзина пуста"));
    }
}
