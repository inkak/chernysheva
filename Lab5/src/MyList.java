import java.util.*;

/**
 * Created by inkak on 4/22/2016.
 */
public class MyList implements List {
    private int size;
    private int nextElem=0;
    public Object[] myList;

    MyList(Object[] myList){
        this.myList=myList;
    }

    public MyList(){
        myList = new Object[10];
    }


    interface ListIterator extends java.util.Iterator<Integer> { }

    private class MyListIterator implements ListIterator{

        @Override
        public boolean hasNext() {
            return (nextElem<=size-1);
        }

        @Override
        public Integer next() {
            Integer myIter=Integer.valueOf((Integer) myList[nextElem]);
            nextElem += 1;
            return myIter;

        }
    }

    @Override
    public int size() {
        return size=myList.length;
    }

    @Override
    public boolean isEmpty() {
        if(size()==0)
            return true;
        else
            return false;
    }

    @Override
    public boolean contains(Object o) {
        for(int i=0; i<size(); i++){
            if(o.equals(myList[i]))
                return true;
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        if (myList.length - size <= 5) {
            increaseListSize();
        }
        //myList[size++] = o;
        return true;
    }

    private void increaseListSize(){
        myList = Arrays.copyOf(myList, myList.length*2);
        System.out.println("New length: "+myList.length);
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Object get(int index) {
        if(index < size){
            return myList[index];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }

    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    @Override
    public void add(int index, Object element) {

    }


    @Override
    public Object remove(int index) {
        if(index < size){
            Object obj = myList[index];
            myList[index] = null;
            int tmp = index;
            while(tmp < size){
                myList[tmp] = myList[tmp+1];
                myList[tmp+1] = null;
                tmp++;
            }
            size--;
            return obj;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
          for (int i = 0; i < myList.length; i++)
            if (myList[i]==null)
            return i;
            } else {
            for (int i = 0; i < myList.length; i++)
             if (o.equals(myList[i]))
             return i;
            }
    return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public java.util.ListIterator listIterator() {
        return null;
    }

    @Override
    public java.util.ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        ListIterator myListIterator=new MyListIterator();
        //Iterator<?> e = c.iterator();
        while (myListIterator.hasNext())
        if (!contains(myListIterator.next()))
            return false;
        return true;
    }

    @Override
    public Object[] toArray(Object[] a) {
      return  new Object[0];
    }
}
