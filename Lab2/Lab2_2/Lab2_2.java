package Lab2_2;

import java.io.IOException;

/**
 * Created by Inka on 08.04.2016.
 */

public class Lab2_2 {
    public static void main(String[] args)  throws IOException {

        String str1 = "4";
        byte a1 = Byte.parseByte(str1);
        System.out.println(a1);

        str1=Byte.toString(a1);
        System.out.println(str1);

        String str2 = "78";
        short a2 = Short.parseShort(str2);
        System.out.println(a2);

        str2=Short.toString(a2);
        System.out.println(str2);

        String str3 = "6767";
        int a3 = Integer.parseInt(str3);
        System.out.println(a3);

        str3=Integer.toString(a3);
        System.out.println(str3);

        String str4 = "978979879879";
        long a4 = Long.parseLong(str4);
        System.out.println(a4);

        str4=Long.toString(a4);
        System.out.println(str4);

        String str5 = "98.7657676";
        float a5 = Float.parseFloat(str5);
        System.out.println(a5);

        str5=Float.toString(a5);
        System.out.println(str5);

        String str6 = "368.6e10";
        double a6 = Double.parseDouble(str6);
        System.out.println(a6);

        str6=Double.toString(a6);
        System.out.println(str6);

        String a11 = "True";
        String a12 = "False";
        boolean b1, b2;
        b1=Boolean.parseBoolean(a11);
        b2=Boolean.parseBoolean(a12);
        System.out.println(b1);
        System.out.println(b2);
        a11=Boolean.toString(b1);
        a12=Boolean.toString(b2);
        System.out.println(a11);
        System.out.println(a12);

        char ch = 'S';
        String ch1 = Character.toString(ch);
        System.out.println(ch1);
        char ch2 = ch1.charAt(0);
        System.out.println(ch2);

    }
}
