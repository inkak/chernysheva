package Lab2_1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Created by inkak on 4/8/2016.
 */
public class Lab2_1 {

    public static void main(String[] args) throws IOException {
        //Arithmetic
        int x=2;
        int y=3;
        float c=(float)(x+x*y)/y;
        System.out.println("Arithmetic: " + c);

        //Comparison
        System.out.print("Comparison: ");
        System.out.println("Please Enter 2 numbers each on separate line:");
        BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));
        String a = br.readLine();
        String b = br.readLine();
        int a1=Integer.parseInt(a);
        int b1=Integer.parseInt(b);
        
        if (a1>b1)
            System.out.println("A bigger then B");
        else if(b1>a1)
            System.out.println("B bigger then A");
        else if(a1==b1)
            System.out.println("You've entered 2 equal numbers");

        //Logical
        System.out.println("Logical:");
        if (a1>b1 && a1>0 && b1>0)
            System.out.println("A bigger then B and they are positive numbers");
        else if(a1>b1 && a1<0 && b1<0)
            System.out.println("A bigger then B and they are negative numbers");
        else if(a1<b1 && b1>0 && b1>0)
            System.out.println("B bigger then A and they are positive numbers");
        else if(a1<b1 && b1<0 && a1<0)
            System.out.println("B bigger then A and they are negative numbers");
        else if(a1>b1 && b1<0 && a1>0)
            System.out.println("A bigger then B and B is negative number");
        else if(a1<b1 && b1>0 && a1<0)
            System.out.println("B bigger then A and A is negative number");
        else if(a1==b1 && a1>0  && b1>0 || b1==a1 && b1>0 && a1>0)
            System.out.println("You've entered 2 equal positive numbers");
        else if(a1==b1 && a1<0  && b1<0 || b1==a1 && b1<0 && a1<0)
            System.out.println("You've entered 2 equal negative numbers");
        else
            System.out.println("Both numbers are zeros");

        //Assimilation
        System.out.print("Assimilation: ");
        a1+=b1;
        System.out.print("Sum of entered numbers: Sum = "+ a1);
    }

    }

