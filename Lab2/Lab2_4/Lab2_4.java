package Lab2_4;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * Created by Inka on 08.04.2016.
 */
public class Lab2_4 {
    public static void main(String[] args) throws IOException {

        //System.out.println("Please enter first number, then math operation, then second number");

        BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter first number and click Enter: ");
        String num1 = reader.readLine();
        System.out.println("Please enter math operation and click Enter: ");
        String operation=reader.readLine();
        System.out.println("Please enter second number and click Enter: ");
        String num2 = reader.readLine();

        double num_1=Double.parseDouble(num1);
        char oper=operation.charAt(0);
        double num_2=Double.parseDouble(num2);

        if(oper=='+') {
            num_1+=num_2;
            if(num_1%1==0) {
                System.out.println("Result: "+(int)num_1);
            }
            else
            System.out.println("Result: "+num_1);
        }
        else if(oper=='-') {
            num_1-=num_2;
            if(num_1%1==0) {
                System.out.println("Result: "+(int)num_1);
            }
            else
            System.out.println("Result: "+num_1);
        }
        else if(oper=='*') {
            num_1*=num_2;
            if(num_1%1==0) {
                System.out.println("Result: "+(int)num_1);
            }
            else
            System.out.println("Result: "+num_1);
        }
        else if(oper=='/') {
            num_1/=num_2;
            if(num_1%1==0) {
                System.out.println("Result: "+(int)num_1);
            }
            else
            System.out.println("Result: "+num_1);
        }
        else if(oper=='%'){
            num_1%=num_2;
            if(num_1%1==0) {
                System.out.println("Result: "+(int)num_1);
            }
            else
            System.out.println("Result: "+num_1);
        }
        }

    }

