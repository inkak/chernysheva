package testsPack.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.TestCase;
import testsPack.runner.Runner;

import static org.junit.Assert.assertTrue;

/**
 * Created by Inka on 07.06.2016.
 */
public class StackoverflowSteps {
    @Given("^I on Stackoverflow main page$")
    public void iOnStackoverflowMainPage() throws Throwable {
        Runner.driver.get("http://stackoverflow.com/");
        assertTrue("We are not on main Stackoverflow page", Runner.mainPage_stack.logoStackTrue.isDisplayed());
    }
    @Then("^I see 'featured' tab count$")
    public void iSeeFeaturedTabCount() throws Throwable {
        Runner.mainPage_stack.checkCount();
        TestCase.assertTrue("Element 'featured tab' wasn't found", Runner.mainPage_stack.count > 300);
        System.out.println("тест прошел");
    }

    @When("^I click on 'sign up' link$")
    public void iClickOnSignUpLink() throws Throwable {
       Runner.signUpPage=Runner.mainPage_stack.signUpClick();
    }

    @And("^I see SignUp page$")
    public void iSeeSignUpPage() throws Throwable {
        assertTrue("We are not on SignUp page", Runner.mainPage_stack.signUpPageTrue.isDisplayed());
        Runner.signUpPage.buttonList();
    }

    @Then("^I see buttons 'Google', 'Facebook'$")
    public void iSeeButtonsGoogleFacebook() throws Throwable {
        assertTrue("Buttons are not found", Runner.signUpPage.buttons.contains("Google") && Runner.signUpPage.buttons.contains("Facebook"));
    }

    @When("^I click on any question link$")
    public void iClickOnAnyQuestionLink() throws Throwable {
        Runner.questionPage=Runner.mainPage_stack.randomQuestionClick();
    }

    @And("^I see question page$")
    public void iSeeQuestionPage() throws Throwable {
        assertTrue("We are not on Question page", Runner.mainPage_stack.questionPageTrue.isDisplayed());
    }

    @Then("^I see 'asked' label value$")
    public void iSeeAskedLabelValue() throws Throwable {
        assertTrue("Question was asked not today", Runner.questionPage.checkDateQ.getText().equals("today"));
    }

    @Then("^I see job panel$")
    public void iSeeJobPanel() throws Throwable {
        assertTrue("Job panel is not displayed", Runner.mainPage_stack.panelJobTrue.isDisplayed());
    }


    @And("^I see job suggestion more then (\\d+)$")
    public void iSeeJobSuggestionMoreThen(int arg0) throws Throwable {
        assertTrue("There is no job suggestions with such payment", Runner.mainPage_stack.jobSug>100);
    }
}
