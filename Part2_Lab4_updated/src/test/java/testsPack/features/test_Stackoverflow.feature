@test2annotation
  Feature: check stackoverflow.com
    Scenario: check count of 'featured' tab > 300
      Given I on Stackoverflow main page
      Then I see 'featured' tab count
    Scenario: check buttons on the Sign Up page
      Given I on Stackoverflow main page
      When I click on 'sign up' link
      And I see SignUp page
      Then I see buttons 'Google', 'Facebook'
    Scenario: check question asked today
      Given I on Stackoverflow main page
      When I click on any question link
      And I see question page
      Then I see 'asked' label value
    Scenario: check salary suggestion > $100
      Given I on Stackoverflow main page
      Then I see job panel
      And I see job suggestion more then 100