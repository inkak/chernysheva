@test1annotation
Feature: check rozetka.com.ua
  @logo
  Scenario: check logo img is present
    Given I on Rozetka main page
    Then I see logo page title
  @item
  Scenario: check product menu with 'Apple' item
    Given I on Rozetka main page
    Then I see menu item 'Apple'
  @itemMp3
  Scenario: check product menu with 'MP3' text
    Given I on Rozetka main page
    Then I see 'MP3' in poduct menu
  @itemCities
  Scenario: check cities in popup menu
    Given I on Rozetka main page
    When I click on link 'Выберете город'
    And I see the pop-up menu
    Then I see links 'Киев', 'Харьков', 'Одесса'
  @cart
  Scenario: check cart is empty
    Given I on Rozetka main page
    When I click on cart link
    Then I see cart page
    And I see cart is empty
