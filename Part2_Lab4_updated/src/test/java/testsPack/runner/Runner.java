package testsPack.runner;

import Rozetka.CartPage;
import Rozetka.CitiesPopup;
import Stackoverflow.QuestionPage;
import Stackoverflow.SignUpPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Inka on 04.06.2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/testsPack/features",
        glue = "testsPack/steps",
        tags={"@test1annotation,@test2annotation"})
public class Runner {
    public static WebDriver driver;
    public static WebDriver driver_stack;
    public static Rozetka.MainPage mainPage;
    public static CitiesPopup citiesPopup;
    public static CartPage cartPage;
    public static Stackoverflow.MainPage mainPage_stack;
    public static SignUpPage signUpPage;
    public static QuestionPage questionPage;

    @BeforeClass
    public static void beforeClass(){
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        mainPage=new Rozetka.MainPage(driver);
        mainPage_stack=new Stackoverflow.MainPage(driver);
    }

    @AfterClass
    public static void afterClass(){
        driver.close();
    }

}
