package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Inka on 04.06.2016.
 */
public class CitiesPopup {
    private WebDriver driver;

    public CitiesPopup(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindAll(@FindBy(how = How.XPATH, using = ".//div[@id='city-chooser']//a[@class='header-city-i-link novisited']"))
    public List<WebElement> citiesIsDisplayedOther;
    public List<String> cities=new ArrayList<String>();
    public void citiesList(){
        for (WebElement elem: citiesIsDisplayedOther) {
            System.out.println(elem.getText());
            cities.add(elem.getText());
        }
        assertTrue("Can't find 'Киев', 'Одесса', 'Харьков'", cities.contains("Киев") && cities.contains("Одесса") && cities.contains("Харьков"));
    }

}
