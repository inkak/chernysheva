package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Inka on 04.06.2016.
 */
public class MainPage {
    public WebDriver driver;

    public MainPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }
    @FindBy(xpath = ".//*[@id='head_banner_container']//div/ul/li/span[@class='novisited m-tabs-link active']")
    public WebElement mainPageTrue;
    
    @FindBy(xpath = ".//header[@id='body-header']//div[@class='logo']/img")
    public WebElement logoRozetka;

    @FindBy(xpath = ".//ul[@id='m-main']//a[contains(@href,'apple')]")
    public WebElement productCatalog;

    @FindBy(xpath = ".//*[@id='m-main']//a[contains(text(), 'MP3')]")
    public WebElement itemMenu;

    @FindBy(xpath = ".//*[@id='city-chooser']//div[@class='popup-css header-city-choose-popup']")
    public WebElement popUpTrue;

    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement cityClick;

    public CitiesPopup navigateToPopup(){
        cityClick.click();
        return new CitiesPopup(driver);
    }

    @FindBy(xpath = ".//*[@id='cart_popup_header']//a[contains(@href,'cart')]")
    public WebElement cart;

    @FindBy(xpath = ".//*[@id='cart-popup']")
    public WebElement cartPageTrue;

    public CartPage navigateToCartPage(){
        cart.click();
        return new CartPage(driver);
    }
}
