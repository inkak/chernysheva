package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Inka on 04.06.2016.
 */
public class QuestionPage {
    private WebDriver driver;

    public QuestionPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*[@id='qinfo']/tbody/tr/td/p/b")
    public WebElement checkDateQ;

}
