import APIMethods.ReadFile;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by inkak on 6/21/2016.
 */
public class testAPI {

    @Test
    public void test1() throws IOException {
        ReadFile readFile=new ReadFile();
        readFile.fileReader();
       for (int i = 0; i < readFile.urlList.size(); i++) {
            assertTrue("Validation is failed", readFile.validation(readFile.expectedList.get(i), readFile.connection(readFile.urlList.get(i))));
        }

    }
}
