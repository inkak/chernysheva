package APIMethods;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by inkak on 6/21/2016.
 */
public class ReadFile {
    public List<String> lines = new ArrayList<String>();
    public List<String> urlList = new ArrayList<String>();
    public List<String> expectedList = new ArrayList<String>();
        public String[] s;


    public void fileReader() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("testFile.txt"));
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        for (int i = 0; i < lines.size(); i++) {
            s=lines.get(i).split("   ");
            urlList.add(s[0]);
            expectedList.add(s[1]);
        }
    }

    public HttpURLConnection connection(String connStr) throws IOException {
                URL url=new URL(connStr);
                HttpURLConnection conn=(HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                if(conn.getResponseCode()!=203){
                    throw new RuntimeException("Failed: HTTP error code");
                }
                System.out.println("connected");
        return conn;
    }

    public boolean validation(String expected, HttpURLConnection conn) throws IOException {

        BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;
        System.out.println("Output from server ... \n");
        String temp="";
        while ((output =br.readLine())!=null){
            temp+=output;
        }
        boolean expectedTrue;
            if(temp.contains(expected)){
                System.out.println("expected phrase contains in your response");
                expectedTrue=true;
            }
            else{
                expectedTrue=false;
            }
        return expectedTrue;
    }

}
