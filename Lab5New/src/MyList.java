import java.util.*;

/**
 * Created by inkak on 4/22/2016.
 */
public class MyList<T> implements List {
    private int size;
    private int nextElem=0;
    int lastRet = -1;
    public Object[] myList;
    private int modCount=0;

    public MyList(Object[] myList) {
        this.myList = myList;
        this.size = myList.length;
    }

    @Override
    public int size() {
        return size=myList.length;
    }

    @Override
    public boolean isEmpty() {
        if(size()==0)
            return true;
        else
            return false;
    }

    @Override
    public boolean contains(Object o) {
        for(int i=0; i<size(); i++){
            if(o.equals(myList[i]))
                return true;
        }
        return false;
    }

    @Override
    public Iterator<Object> iterator() {
        return new Iter();
    }

    private class Iter implements Iterator<Object> {

        int expectedModCount = modCount;

        @Override
        public Object next() {
            return myList[nextElem++];
        }

        @Override
        public boolean hasNext() {
            return nextElem < size && myList[nextElem] != null;
        }

        @Override
        public void remove() {
            if (lastRet < nextElem)
                nextElem--;
            lastRet = -1;
            expectedModCount = modCount;
        }
    }


    @Override
    public Object[] toArray() {
        return  Arrays.copyOf(myList, size);
    }

    @Override
    public boolean add(Object o) {
        if (myList.length - size <= 5) {
            addSize();
        }
        myList[size++] = o;
        return true;
    }

    private void addSize(){
        myList = Arrays.copyOf(myList, myList.length*2);
        System.out.println("New length: "+myList.length);
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (int i = 0; i < myList.length; i++)
                if (myList[i] == null)
                    remove(i);
            return true;
        } else {
            for (int i = 0; i < myList.length; i++)
                if (o.equals(myList[i]))
                    remove(i);
            return true;
        }
    }
    @Override
    public boolean addAll(Collection c) {
        boolean add = false;
        Iterator<MyList> addAll = c.iterator();
        while (addAll.hasNext()) {
            if (add(addAll.next()))
                add = true;
        }
        return add;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        boolean add = false;

        for (int i = 0; i < myList.length; i++) {
            if(index<size ){

                myList[index] = c.toArray();
                add = true;
            }

        }
        return add;
    }



    @Override
    public void clear() {
        for (int i = 0; i < size(); i++) {
            myList[i] = null;
        }
        nextElem = 0;
    }

    @Override
    public Object get(int index) {
        if(index < size){
            return myList[index];
        } else {
            return -1;
        }

    }

    @Override
    public Object set(int index, Object element) {
        if(index<size){
            myList[index]=element;
        }
        return element;
    }

    @Override
    public void add(int index, Object element) {
        if(index<size){
            System.arraycopy(myList, index, myList, index + 1, size - index);
            myList[index] = element;
            size++;
        }
    }


    @Override
    public Object remove(int index) {
        Object oldVal=myList[index];
        if(index < size){
            int numMoved = size - index - 1;
            System.arraycopy(myList, index + 1, myList, index, numMoved);
            myList[--size] = null;
        }
        return oldVal;
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < myList.length; i++)
                if (myList[i]==null)
                    return i;
        } else {
            for (int i = 0; i < myList.length; i++)
                if (o.equals(myList[i]))
                    return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = myList.length; i > 0; i--)
                if (myList[i]==null)
                    return i;
        }else{
            for (int i = myList.length-1; i > 0; i--)
                if (o.equals(myList[i]))
                    return i;
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return listIterator(0);
    }

    private class ListIter extends Iter implements ListIterator<Object> {

        ListIter(int index) {
            nextElem = index;
            }
        @Override
        public boolean hasPrevious() {
            return nextElem != 0;
        }

        @Override
        public Object previous() {
            int i = nextElem - 1;
            Object previous = get(i);
            lastRet = nextElem = i;
            return previous;
        }

        @Override
        public int nextIndex() {
            return nextElem;
        }

        @Override
        public int previousIndex() {
            return nextElem-1;
        }

        @Override
        public void set(Object o) {
            MyList.this.set(lastRet, o);
            expectedModCount = modCount;
        }

        @Override
        public void add(Object o) {
            int i = nextElem;
            MyList.this.add(i, o);
            nextElem = i + 1;
            lastRet = -1;
            expectedModCount = modCount;
        }
    }

    @Override
    public ListIterator listIterator(int index) {
        return new ListIter(index);
    }

       @Override
    public List subList(int fromIndex, int toIndex) {

           Object[] newMyListMas=new Object[toIndex-fromIndex];

           if (fromIndex < size && toIndex < size) {
               System.arraycopy(myList, fromIndex, newMyListMas, 0, toIndex - fromIndex-1);
               }
           return new MyList(newMyListMas);
           }


    @Override
    public boolean retainAll(Collection c) {
        boolean changed = false;
        for (int i = size() - 1; i >= 0; i--) {
            Object obj = get(i);
            if (!c.contains(obj)) {
                remove(i);
                changed = true;
            }
        }
        return changed;
    }




    @Override
    public boolean removeAll(Collection c) {
        Iterator<MyList> removeAll=c.iterator();
        while(removeAll.hasNext()){
            if(!remove(removeAll.next()))
                return false;
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        Iterator<MyList> iter=c.iterator();
        while(iter.hasNext()){
            if(!contains(iter.next()))
                return false;
        }
        return true;
    }

    @Override
    public Object[] toArray(Object[] a) {
        Object[] objects = a;
        for (Object obj : objects)
            System.out.print(obj+" ");
        return  a;
    }
}