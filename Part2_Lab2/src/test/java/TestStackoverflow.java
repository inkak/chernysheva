import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by inkak on 5/26/2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private String url1="http://stackoverflow.com/";

    @BeforeClass
    public static void beforeAll() throws Exception{
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        System.out.println("Открыли браузер, Расширили окно");
    }
    @Before
    public void before() throws Exception{
        driver.navigate().refresh();
        driver.get(url1);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.out.println("Загрузили линку, подождали");
    }

    @AfterClass
    public static void afterAll() throws Exception {
        driver.close();
        System.out.println("Закрыли браузер");
    }

    @Test
    public void test1_CheckFeaturedCount() throws Exception {
        WebElement featuredCount = driver.findElement(By.xpath(".//*[@id='tabs']//span"));
        String s=featuredCount.getText();
        int count=Integer.parseInt(s);
        assertTrue("Element 'featured tab' wasn't found", count > 300);
        System.out.println("Тест 1");
    }

    @Test
    public void test2_CheckSignUp() throws Exception{
        WebElement signUp = driver.findElement(By.xpath("html/body//span/a[contains(text(),'sign up')]"));
        signUp.click();
        List<WebElement>checkButtons=driver.findElements(By.xpath(".//*[@id='openid-buttons']//div[@class='text']/span"));
        for (WebElement elem: checkButtons) {
            assertTrue("Buttons are not found", elem.isDisplayed());
        }
        System.out.println("Тест 2");
    }

    @Test
    public void test3_CheckQuestionClick() throws Exception{
        List<WebElement> questionClick=driver.findElements(By.xpath(".//div[contains(@class,'question-summary')]//h3/a"));
        int randomRec=(new Random()).ints(1, questionClick.size()).iterator().nextInt();
        int i=0;
        for (WebElement elem: questionClick) {
            if(i==randomRec) {
                elem.click();
                System.out.println(i+" "+randomRec);
                break;
            }
            else
                i++;
        }
        WebElement checkDateQ=driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr/td/p/b"));
        assertTrue("Question was asked not today", checkDateQ.getText().equals("today"));
        System.out.println("Тест 3");
    }

    @Test
    public void test4_CheckSalarySuggestion() throws Exception{
        List<WebElement> pathsList = driver.findElements(By.xpath(".//div[@id='hireme']//div[@class='title']"));
        int [] x=new int[pathsList.size()];
        int i=0;
        for(WebElement elem: pathsList){
            String s1=elem.getText().replaceAll("[^\\$(\\d*)]", "");
            String s2=s1.trim().replaceAll("[^0-9]", "");
            if(s2.equals("")){
                x[i]=0;
                i++;
            }
            else{
                int pay=Integer.parseInt(s2);
                x[i]=pay;
                i++;
            }
        }
        int jobSug=0;
        for(int j=0;j<x.length;j++){
            if(x[j]>100){
                jobSug=x[j];
            }
        }
        assertTrue("There is no job suggestions with such payment", jobSug>100);
    }

}