package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Inka on 04.06.2016.
 */
public class SignUpPage {
    private WebDriver driver;

    public SignUpPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindAll(@FindBy(how = How.XPATH, using = ".//*[@id='openid-buttons']//div[@class='text']/span"))
    public List<WebElement> checkButtons;
    public List<String> buttons=new ArrayList<String>();
    public void buttonList(){
        for (WebElement elem: checkButtons) {
            buttons.add(elem.getText());
        }
    }

}
