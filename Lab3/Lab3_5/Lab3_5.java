package Lab3_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by inkak on 4/13/2016.
 */
public class Lab3_5 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;
        while(true) {
            String s = reader.readLine();
            if ("Сумма".equals(s)) {
                System.out.println(sum);
                break;
            }
            else {
                sum += Integer.parseInt(s);
            }
        }
    }
}
