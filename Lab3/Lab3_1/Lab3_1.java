package Lab3_1;

/**
 * Created by inkak on 4/12/2016.
 */
public class Lab3_1 {
    public static void main(String[] args) {
        int [] x = {5, 8, 6, 3};
        int min = x[0];
        for(int i = 0; i < x.length; i++){
            if( min > x[i])
                min = x[i];
        }
        System.out.println("Минимальный элемент массива: "+min);
    }
}
