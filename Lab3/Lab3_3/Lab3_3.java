package Lab3_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;

import static java.lang.Math.sqrt;

/**
 * Created by inkak on 4/12/2016.
 */
public class Lab3_3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String num = reader.readLine();
        int num_1 = Integer.parseInt(num);
        System.out.print(2+" ");
        boolean flag = false;
        for(int i=3; i<=num_1; i=i+2){
            for(int j=2; j<=sqrt(i); j++){
                if (i%j!=0){
                    flag=true;
                }
                else {
                    flag=false;
                    break;
                }
            }
            if(flag) {
                System.out.print(i + " ");
            }
        }
    }
}
