import org.junit.Assert;

import java.io.*;
import java.lang.reflect.Method;
import static org.junit.Assert.*;
/**
 * Created by Inka on 09.05.2016.
 */
public class Test {
    @org.junit.Test
    public void testSerialization() throws Exception {
        FileOutputStream myFileOS = new FileOutputStream("temp.out");
        ObjectOutputStream myOS = new ObjectOutputStream(myFileOS);
        MySeriaClass mySeria=new MySeriaClassChild();
        myOS.writeObject(mySeria);
        Assert.assertTrue(new File("temp.out").exists());
    }



    @org.junit.Test
    public void testDeserialization() throws Exception{
        FileInputStream myInput=new FileInputStream("temp.out");
        ObjectInputStream myIS=new ObjectInputStream(myInput);
        MySeriaClassChild mySeria1=(MySeriaClassChild) myIS.readObject();
        Assert.assertTrue(new File("temp.out").exists());
    }
    @org.junit.Test
    public void testReflection() throws Exception{
        MySeriaClass mySeria=new MySeriaClassChild();
        Class c=mySeria.getClass();
        Class superClass=c.getSuperclass();
        Class[] paramsTypes=new Class[]{int.class};
        Method m=superClass.getDeclaredMethod("Sum", paramsTypes);
        m.setAccessible(true);
        m.invoke(mySeria, 15);
        Assert.assertTrue("Method is exicted", m.getName().equals("Sum"));
    }

}
