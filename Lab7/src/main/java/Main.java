import com.sun.deploy.services.Service;
import com.sun.xml.internal.ws.developer.Serialization;
import jdk.nashorn.internal.ir.RuntimeNode;
import sun.misc.Request;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Inka on 08.05.2016.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Serialization();
        Deserialization();
        Reflection();
    }
    public static void Serialization() throws Exception {
        FileOutputStream myFileOS = new FileOutputStream("temp.out");
        ObjectOutputStream myOS = new ObjectOutputStream(myFileOS);
        MySeriaClass mySeria=new MySeriaClassChild();
        myOS.writeObject(mySeria);
    }

    public static void Deserialization() throws Exception{
        FileInputStream myInput=new FileInputStream("temp.out");
        ObjectInputStream myIS=new ObjectInputStream(myInput);
        MySeriaClass mySeria1=(MySeriaClassChild) myIS.readObject();
    }
    public static void Reflection() throws Exception{
        MySeriaClass mySeria=new MySeriaClassChild();
        Class c=mySeria.getClass();
        Class superClass=c.getSuperclass();
        Class[] paramsTypes=new Class[]{int.class};
        Method m=superClass.getDeclaredMethod("Sum", paramsTypes);
        m.setAccessible(true);
        m.invoke(mySeria, 15);


    }

}
