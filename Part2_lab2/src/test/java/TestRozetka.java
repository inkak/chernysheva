import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class TestRozetka {
    private static WebDriver driver;
    private String url="http://rozetka.com.ua/";

    @BeforeClass
    public static void beforeAll() throws Exception{
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.out.println("Открыли браузер, Расширили окно");
    }

    @Before
    public void before() throws Exception{
        driver.navigate().refresh();
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.out.println("Загрузили линку, подождали");
    }

   /* @After
    public void after() throws Exception{
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
        System.out.println("Работа в новой вкладке");
    }*/

    @AfterClass
    public static void afterAll() throws Exception{
        driver.close();
        System.out.println("Закрыли браузер");
    }

    @Test
    public void test1_CheckLogoRozetka() throws Exception{
        WebElement logoRozetka=driver.findElement(By.xpath(".//header[@id='body-header']//div[@class='logo']/img"));
        assertTrue("Element 'logo Rozetka' wasn't found", logoRozetka.isDisplayed());
        System.out.println("Тест 1");
    }
    @Test
    public void test2_CheckProductCatalog() throws Exception{
        WebElement productCatalog=driver.findElement(By.xpath(".//ul[@id='m-main']//a[contains(@href,'apple')]"));
        assertTrue("Item of menu 'Apple' doesn't exist", productCatalog.getText().contains("Apple"));
        System.out.println("Тест 2");
    }
    @Test
    public void test3_menuItemMP3() throws Exception{
        WebElement item=driver.findElement(By.xpath(".//*[@id='m-main']//a[contains(text(), 'MP3')]"));
        assertTrue("Can't find 'MP3' element in menu", item.getText().contains("MP3"));
        System.out.println("Тест 3");

    }
    @Test
    public void test4_CheckChangeCity() throws Exception{
        WebElement cityClick=driver.findElement(By.xpath(".//*[@id='city-chooser']/a"));
        cityClick.click();
        List<WebElement> citiesIsDisplayedOther=driver.findElements(By.xpath(".//div[@id='city-chooser']//a[@class='header-city-i-link novisited']"));
        List<String> cities=new ArrayList<String>();
        for (WebElement elem: citiesIsDisplayedOther) {
            System.out.println(elem.getText());
            cities.add(elem.getText());
        }
        assertTrue("Can't find 'Киев', 'Одесса', 'Харьков'", cities.contains("Киев") && cities.contains("Одесса") && cities.contains("Харьков"));
        System.out.println("Тест 4");
    }

    @Test
    public  void test5_CheckCartIsEmpty() throws Exception{
        WebElement cart=driver.findElement(By.xpath(".//*[@id='cart_popup_header']//a[contains(@href,'cart')]"));
        cart.click();
        WebElement empty=driver.findElement(By.xpath(".//*[@id='drop-block']/h2[contains(@class,'empty')]"));
        assertTrue("Cart is not empty", empty.getText().contains("Корзина пуста"));
    }
}