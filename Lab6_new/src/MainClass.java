import java.io.*;

/**
 * Created by Inka on 02.05.2016.
 */
public class MainClass{

    public static void main(String[] args) throws IOException {

        int i=1;
        while(true){
            BufferedReader userInputName = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter some word that is contained in any file you want to copy and enter STOP if you want to stop: ");
            String word = userInputName.readLine();
            if(word.equals("STOP"))
                break;
            else new MyThread(word, i).start();
            i++;
        }

    }




}