import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by Inka on 02.05.2016.
 */
public class MyThread extends Thread {
    private static String word;
    private int i;

    public MyThread(String word, int i) {
        this.word = word;
        this.i=i;
    }


    @Override
    public void run() {
        String pathSource=System.getProperty("user.dir");
        String pathDestination=System.getProperty("user.dir");
        File srcFolder = new File(pathSource);
        File destFolder = new File(pathDestination+i);
        //File srcFolder = new File("out/production/Lab6_new/RootFolder");
        //File destFolder = new File("out/production/Lab6_new/RootFolder"+i);
        try {
            copyFolder(srcFolder, destFolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("thread is ended");
    }



    public static void copyFolder(File src, File dest)throws IOException {
        if(src.isDirectory()){
            if(!dest.exists()){
                dest.mkdir();
            }
            String files[] = src.list();
            for (String file : files) {
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                if(srcFile.isDirectory()){
                    copyFolder(srcFile,destFile);
                }
                else{
                    CharSequence cs1 = word;
                    boolean flag;
                    String s=srcFile.getName();
                    if(flag = s.contains(cs1)){
                        copyFolder(srcFile,destFile);
                    }
                }
            }

        }else{
            File source=new File(String.valueOf(src));
            File target=new File(String.valueOf(dest));
            Files.copy(source.toPath(), target.toPath());
            System.out.println("Searched file copied from " + src + " to " + dest);
        }
    }
}
